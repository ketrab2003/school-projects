#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int new_rand(){
    return rand()*RAND_MAX+rand();
}

int main(){
    srand(time(NULL));

    int n = 1000;

    while(n--)
        printf("%d\n", new_rand()%1000000+1);

    return 0;
}